package com.obando.spinnercustom

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setCustomAdapterSpinner()
    }

    fun setCustomAdapterSpinner() {

        val country_list = arrayListOf<Items>()

        country_list.add(Items("India"))
        country_list.add(Items("United States"))
        country_list.add(Items("Indonesia"))
        country_list.add(Items("France"))
        country_list.add(Items("China"))

        val adapter = ItemsAdapters(
            this,
            country_list
        )
        var params : LinearLayout.LayoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, // This will define text view width
            LinearLayout.LayoutParams.WRAP_CONTENT // This will define text view height
        )

        // Add margin to the text view
        params.setMargins(16,16,16,0)

        //Creamos un relativelayout
        val linearLayout:LinearLayout = LinearLayout(this)
        linearLayout.layoutParams = params

        //mejora para visibilidad


        linearLayout.orientation = LinearLayout.VERTICAL
        var paramsSpinner : RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, // This will define text view width
            LinearLayout.LayoutParams.WRAP_CONTENT // This will define text view height
        )

        //paramsSpinner.addRule(RelativeLayout.BELOW,textViewTitulo.id)

        //layout del TextInputEditText de Material
        val spinner: Spinner = Spinner(this)
        spinner.layoutParams = paramsSpinner

        spinner.adapter = adapter

        spinner.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, p1: View?, pos: Int, p3: Long) {
                Toast.makeText(this@MainActivity, "" + (parent?.getItemAtPosition(pos) as Items).titlNam, Toast.LENGTH_SHORT).show()
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }
        })

        linearLayout.addView(spinner)

        //agregamos la vista al contenedor
        contenedor.addView(linearLayout)
        // dynamically adding data after setting adapter to spinner

        country_list.add(Items("Japan"))
        country_list.add(Items("New Zealand djniofsbnfmdmv kn eknds mdvjpod mds i fjsdnvkjdgw knvjshdog wsmdv indgfs mvniosdgns vjnsiod fjnz ionou"))
        country_list.add(Items("Other"))

        adapter.notifyDataSetChanged()
    }
}
