package com.obando.spinnercustom

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.spinner_la.view.*

class ItemsAdapters(ctx: Context, countries: ArrayList<Items>) : ArrayAdapter<Items>(ctx, 0, countries) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return createItemView(position, convertView, parent);
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return createItemView(position, convertView, parent);
    }

    fun createItemView(position: Int, recycledView: View?, parent: ViewGroup):View {
        val country = getItem(position)

        val view = recycledView ?: LayoutInflater.from(context).inflate(
            R.layout.spinner_la,
            parent,
            false
        )

        country?.let {
            view.txtnom.text = country.titlNam
        }
        return view
    }
}